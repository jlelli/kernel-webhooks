"""Webhook interaction tests."""
import copy
import unittest
from unittest import mock

import webhook.commit_compare
import webhook.common

# a hunk of upstream linux kernel commit ID 1fc70edb7d7b5ce1ae32b0cf90183f4879ad421a
PATCH_A = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_A += "index blahblah..blahblah 100644\n"
PATCH_A += "--- a/include/linux/netdevice.h\n"
PATCH_A += "+++ b/include/linux/netdevice.h\n"
PATCH_A += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_A += "        unsigned short          type;\n"
PATCH_A += "        unsigned short          hard_header_len;\n"
PATCH_A += "        unsigned char           min_header_len;\n"
PATCH_A += "+       unsigned char           name_assign_type;\n"
PATCH_A += "\n"
PATCH_A += "        unsigned short          needed_headroom;\n"
PATCH_A += "        unsigned short          needed_tailroom;\n"

PATCH_B = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_B += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_B += "--- a/include/linux/netdevice.h\n"
PATCH_B += "+++ b/include/linux/netdevice.h\n"
PATCH_B += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_B += "        unsigned short          type;\n"
PATCH_B += "        unsigned short          hard_header_len;\n"
PATCH_B += "        unsigned char           min_header_len;\n"
PATCH_B += "+       unsigned char           name_assign_type;\n"
PATCH_B += "\n"
PATCH_B += "        unsigned short          needed_max_headroom;\n"
PATCH_B += "        unsigned short          needed_tailroom;\n"

PATCH_C = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_C += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_C += "--- a/include/linux/netdevice.h\n"
PATCH_C += "+++ b/include/linux/netdevice.h\n"
PATCH_C += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_C += "        unsigned short          type;\n"
PATCH_C += "        unsigned short          hard_header_len;\n"
PATCH_C += "        unsigned char           min_header_len;\n"
PATCH_C += "+       RH_KABI_EXTEND(unsigned char name_assignee_type)\n"
PATCH_C += "\n"
PATCH_C += "        unsigned short          needed_headroom;\n"
PATCH_C += "        unsigned short          needed_tailroom;\n"

PATCH_D = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_D += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_D += "--- a/include/linux/netdevice.h\n"
PATCH_D += "+++ b/include/linux/netdevice.h\n"
PATCH_D += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_D += "        unsigned short          type;\n"
PATCH_D += "        unsigned short          hard_header_len;\n"
PATCH_D += "        unsigned char           min_header_len;\n"
PATCH_D += "+       unsigned char           name_assign_type;\n"
PATCH_D += "\n"
PATCH_D += "        unsigned short          needed_headroom;\n"
PATCH_D += "        unsigned short          needed_tailroom;\n"
PATCH_D += "diff --git a/include/linux/netlink.h b/include/linux/netlink.h\n"
PATCH_D += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_D += "--- a/include/linux/netlink.h\n"
PATCH_D += "+++ b/include/linux/netlink.h\n"
PATCH_D += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_D += "        unsigned short          type;\n"
PATCH_D += "        unsigned short          hard_header_len;\n"
PATCH_D += "        unsigned char           min_header_len;\n"
PATCH_D += "+       unsigned char           name_assign_type;\n"
PATCH_D += "\n"
PATCH_D += "        unsigned short          needed_headroom;\n"
PATCH_D += "        unsigned short          needed_tailroom;\n"


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommitCompare(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p'},
                     'object_attributes': {'target_branch': 'main', 'iid': 2},
                     'changes': {'labels': {'previous': [],
                                            'current': []}},
                     'state': 'opened'
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'note': 'comment',
                                          'noteable_type': 'MergeRequest'},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.common.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab, udiff, sdiff):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        udiff.return_value = "UPSTREAM_DIFF_OUTPUT"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT"
        self._test_payload(mocked_gitlab, False, payload=payload)

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.common.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab, udiff, sdiff):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = '8.3-net'
        payload['object_attributes']['action'] = 'open'
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT"
        self._test_payload(mocked_gitlab, True, payload=payload)

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.common.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_note(self, mocked_gitlab, udiff, sdiff):
        """Check handling of a note."""
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT"
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_NOTE)

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.common.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_ucid_re_evaluation(self, mocked_gitlab, udiff, sdiff):
        """Check handling of commit ID re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-commit-id-evaluation"
        payload["object_attributes"]["noteable_type"] = "MergeRequest"
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT"
        self._test_payload(mocked_gitlab, True, payload)

    def test_get_match_info(self):
        """Check that we get sane strings back for match types."""
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.FULL)
        self.assertEqual(output, ("100% match", True))
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.PARTIAL)
        self.assertEqual(output, ("Partial   ", False))
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.DIFFS)
        self.assertEqual(output, ("Diffs     ", False))
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.KABI)
        self.assertEqual(output, ("kABI Diffs", False))
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.RHELONLY)
        self.assertEqual(output, ("n/a       ", False))
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.POSTED)
        self.assertEqual(output, ("n/a       ", False))
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.BADMAIL)
        self.assertEqual(output, ("Bad email ", False))

    def test_find_kabi_hints(self):
        """Check that we can find a kabi hint in a patch hunk."""
        output = webhook.commit_compare.find_kabi_hints(PATCH_A)
        self.assertFalse(output)
        output = webhook.commit_compare.find_kabi_hints(PATCH_C)
        self.assertTrue(output)

    def test_ucid_compare(self):
        """Check that diff engine actually compares things properly."""
        # compare two identical patches (should be equal)
        output = webhook.common.compare_commits(PATCH_A, PATCH_A)
        self.assertEqual(output, [])
        # compare two patches with only context differences (should be equal)
        output = webhook.common.compare_commits(PATCH_A, PATCH_B)
        self.assertEqual(output, [])
        # compare two patches with actual differences (should NOT be equal)
        output = webhook.common.compare_commits(PATCH_A, PATCH_C)
        self.assertNotEqual(output, [])

    def test_no_ucid_commit_note(self):
        """Test that we get the expected data back for commits w/o an upstream commit ID."""
        mri = mock.Mock(notes=[], noucid_id=0)
        output = webhook.commit_compare.process_no_ucid_commit(mri)
        self.assertEqual(mri.notes[0], webhook.commit_compare.noucid_msg())
        self.assertEqual(mri.noucid_id, '1')
        self.assertEqual(output['match'], webhook.commit_compare.Match.NOUCID)

    def test_rhelonly_commit_note(self):
        """Test that we get the expected data back for commits that are RHEL-only."""
        mri = mock.Mock(notes=[], rhelonly_id=0)
        output = webhook.commit_compare.process_rhel_only_commit(mri)
        self.assertEqual(mri.notes[0], webhook.commit_compare.rhelonly_msg())
        self.assertEqual(mri.rhelonly_id, '1')
        self.assertEqual(output['match'], webhook.commit_compare.Match.RHELONLY)

    def test_posted_commit_note(self):
        """Test that we get the expected data back for commits that are RHEL-only."""
        mri = mock.Mock(notes=[], posted_id=0)
        output = webhook.commit_compare.process_posted_commit(mri)
        self.assertEqual(mri.notes[0], webhook.commit_compare.posted_msg())
        self.assertEqual(mri.posted_id, '1')
        self.assertEqual(output['match'], webhook.commit_compare.Match.POSTED)

    def test_unknown_commit_note(self):
        """Test that we get the expected data back for commits that are of unknown origin."""
        mri = mock.Mock(notes=[], unknown_id=0)
        output = webhook.commit_compare.process_unknown_commit(mri)
        self.assertEqual(mri.notes[0], webhook.commit_compare.unk_cid_msg())
        self.assertEqual(mri.unknown_id, '1')
        self.assertEqual(output['match'], webhook.commit_compare.Match.NOUCID)

    def test_kabi_commit_note(self):
        """Test that we get the expected data back for commits that flagged for kabi screening."""
        mri = mock.Mock(notes=[], kabi_id=0)
        output = webhook.commit_compare.process_kabi_patch(mri)
        self.assertEqual(mri.notes[0], webhook.commit_compare.kabi_msg())
        self.assertEqual(mri.kabi_id, '1')
        self.assertEqual(output['match'], webhook.commit_compare.Match.KABI)

    def test_commit_with_diffs_note(self):
        """Test that we get the expected data back for commits with diffs from upstream."""
        mri = mock.Mock(notes=[], diffs_id=0)
        output = webhook.commit_compare.process_commit_with_diffs(mri)
        self.assertEqual(mri.notes[0], webhook.commit_compare.diffs_msg())
        self.assertEqual(mri.diffs_id, '1')
        self.assertEqual(output['match'], webhook.commit_compare.Match.DIFFS)

    def test_partial_commit_note(self):
        """Test that we get the expected data back for partial backport commits."""
        mri = mock.Mock(notes=[], part_id=0)
        output = webhook.commit_compare.process_partial_backport(mri)
        self.assertEqual(mri.notes[0], webhook.commit_compare.partial_msg())
        self.assertEqual(mri.part_id, '1')
        self.assertEqual(output['match'], webhook.commit_compare.Match.PARTIAL)

    def test_invalid_author_note(self):
        """Test that we get the expected data back for commits with invalid emails."""
        mri = mock.Mock(notes=[], badmail_id=0)
        output = webhook.commit_compare.process_invalid_author_commit(mri)
        self.assertEqual(mri.notes[0], webhook.commit_compare.badmail_msg())
        self.assertEqual(mri.badmail_id, '1')
        self.assertEqual(output['match'], webhook.commit_compare.Match.BADMAIL)

    def test_get_submitted_diff(self):
        """Test that we can properly extract patch content from a submitted diff."""
        commit = []
        path = {'old_path': 'include/linux/netdevice.h',
                'new_path': 'include/linux/netdevice.h',
                'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                        "        unsigned short          type;\n"
                        "        unsigned short          hard_header_len;\n"
                        "        unsigned char           min_header_len;\n"
                        "+       unsigned char           name_assign_type;\n"
                        "\n"
                        "        unsigned short          needed_headroom;\n"
                        "        unsigned short          needed_tailroom;\n"}
        commit.append(path)
        output = webhook.common.get_submitted_diff(commit)
        self.assertEqual(output[0], PATCH_A)
        self.assertEqual(output[1], ["include/linux/netdevice.h"])

    def test_partial_diff(self):
        """Check that we can match partial diffs."""
        filelist = []
        filelist.append("include/linux/netdevice.h")
        pdiff = webhook.common.get_partial_diff(PATCH_D, filelist)
        output = webhook.common.compare_commits(pdiff, PATCH_A)
        self.assertEqual(output, [])

    def test_valid_rhcommit_author(self):
        """Check that we're properly validating commit authors."""
        # Format: "rh commit author email", "upstream commit author email", "mr creator email"
        t1 = ["jdoe@redhat.com", "upstream@example.com", "jdoe@redhat.com"]
        t2 = ["jdoe@redhat.com", "upstream@example2.com", "contrib@example.com"]
        t3 = ["contrib@example.com", "upstream@example2.com", "contrib@example.com"]
        t4 = ["upstream@example.com", "upstream@example.com", "jdoe@redhat.com"]
        self.assertTrue(webhook.commit_compare.valid_rhcommit_author(t1[0], t1[1], t1[2]))
        self.assertTrue(webhook.commit_compare.valid_rhcommit_author(t2[0], t2[1], t2[2]))
        self.assertTrue(webhook.commit_compare.valid_rhcommit_author(t3[0], t3[1], t3[2]))
        self.assertFalse(webhook.commit_compare.valid_rhcommit_author(t4[0], t4[1], t4[2]))

    def test_has_upstream_commit_hash(self):
        """Check that we get expected return values from different match types."""
        m1 = webhook.commit_compare.Match.FULL
        m2 = webhook.commit_compare.Match.PARTIAL
        m3 = webhook.commit_compare.Match.DIFFS
        m4 = webhook.commit_compare.Match.NOUCID
        m5 = webhook.commit_compare.Match.RHELONLY
        m6 = webhook.commit_compare.Match.POSTED
        self.assertTrue(webhook.commit_compare.has_upstream_commit_hash(m1))
        self.assertTrue(webhook.commit_compare.has_upstream_commit_hash(m2))
        self.assertTrue(webhook.commit_compare.has_upstream_commit_hash(m3))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m4))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m5))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m6))

    @mock.patch('webhook.commit_compare.get_mri')
    @mock.patch('webhook.commit_compare.perform_mri_tasks')
    def test_process_mr_no_label_changes(self, mock_get_mri, mock_perform_mri_tasks):
        msg = mock.Mock()
        msg.payload = {'object_kind': 'merge_request',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p'},
                       'object_attributes': {'iid': 2,
                                             'action': 'update'},
                       'changes': {'labels': {'previous': [{'title': 'CommitRefs::OK'}],
                                              'current': [{'title': 'CommitRefs::OK'}]}}
                       }
        webhook.commit_compare.process_mr(None, msg, 'linux-src')
        mock_get_mri.assert_not_called()
        mock_perform_mri_tasks.assert_not_called()

    def _test_payload(self, mocked_gitlab, result, payload):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, [])
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, [])

    # pylint: disable=too-many-arguments
    def _test(self, mocked_gitlab, result, payload,
              labels, assert_labels=None):

        # setup dummy gitlab data
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.Mock(target_branch=target)
        merge_request.author = {'id': 1}
        merge_request.iid = 2
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        c2 = mock.Mock(id="4567", author_email="xyz@example.com", message="2\ncommit 12345678")
        c3 = mock.Mock(id="890a", author_email="jdoe@redhat.com", message="3\nUpstream: RHEL-only")
        c4 = mock.Mock(id="deadbeef", author_email="zzzzzz@redhat.com", message="4\n"
                       "(cherry picked from commit abcdef0123456789abcdef0123456789abcdef01)")
        c5 = mock.Mock(id="0ff0ff00", author_email="spam@redhat.com", message="5\nUpstream: Posted")
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        project.labels.list.return_value = []
        merge_request.labels = labels
        merge_request.commits.return_value = [c1, c2, c3, c4]
        merge_request.pipeline = {'status': 'success'}
        gl_instance = mock.Mock()
        gl_instance.users.get.return_value = mock.Mock(public_email="jdoe@redhat.com")
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "deadbeef": c4, "00f00f00": c5}
        self.assertEqual(webhook.commit_compare.extract_ucid(c1.message),
                         ["1234567890abcdef1234567890abcdef12345678"])
        self.assertEqual(webhook.commit_compare.extract_ucid(c2.message), [])
        self.assertEqual(webhook.commit_compare.extract_ucid(c3.message), ["RHELonly"])
        self.assertEqual(webhook.commit_compare.extract_ucid(c4.message),
                         ["abcdef0123456789abcdef0123456789abcdef01"])
        self.assertEqual(webhook.commit_compare.extract_ucid(c5.message), ["Posted"])

        # setup dummy post results data
        presult = mock.Mock()
        presult.json.return_value = {
            "error": "*** No upstream commit ID found in submitted patch!\n***",
            "result": "fail",
            "logs": "*** Comparing submitted patch with upstream commit ID\n***"
        }

        with mock.patch('webhook.commit_compare.SESSION.post', return_value=presult):
            return_value = \
                webhook.common.process_message('dummy', payload,
                                               webhook.commit_compare.WEBHOOKS,
                                               False, linux_src='/src/linux')

        if result:
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN',
                session=mock.ANY)
            mocked_gitlab().__enter__().projects.get.assert_called_with(1)
            project.mergerequests.get.assert_called_with(2)
            self.assertTrue(return_value)
            if assert_labels:
                self.assertEqual(sorted(merge_request.labels), sorted(assert_labels))
        else:
            self.assertFalse(return_value)
