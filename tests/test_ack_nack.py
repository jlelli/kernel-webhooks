"""Webhook interaction tests."""
import copy
import unittest
from unittest import mock

import webhook.ack_nack
import webhook.common


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestAckNack(unittest.TestCase):
    PAYLOAD_NOTE = {'object_kind': 'note',
                    'user': {'id': 1, 'username': 'user1'},
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'iid': 1,
                                          'noteable_type': 'MergeRequest',
                                          'note': 'comment',
                                          'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    @mock.patch('webhook.ack_nack._run_get_maintainers')
    def test_get_required_reviewers(self, get_maint):
        maint_stdout = 'User 1 <user1@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'User 2 <user2@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'somelist@redhat.com (open list:SUBSYSTEM LIST)\n'
        get_maint.return_value = (0, maint_stdout)

        reviewers = webhook.ack_nack._get_required_reviewers(['mocked'], 'mocked',
                                                             'user1@redhat.com')
        self.assertEqual(reviewers, {frozenset(['user1@redhat.com', 'user2@redhat.com'])})

        reviewers = webhook.ack_nack._get_required_reviewers([], 'mocked', 'user1@redhat.com')
        self.assertEqual(reviewers, set([]))

    @mock.patch('webhook.ack_nack._run_get_maintainers')
    def test_get_required_reviewers_no_self_review(self, get_maint):
        # The person that opened the merge request is the only person listed as a subsystem
        # maintainer. That person should not be listed as a required reviewer.
        maint_stdout = 'User 1 <user1@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'somelist@redhat.com (open list:SUBSYSTEM LIST)\n'
        get_maint.return_value = (0, maint_stdout)

        reviewers = webhook.ack_nack._get_required_reviewers(['mocked'], 'mocked',
                                                             'user1@redhat.com')
        self.assertEqual(reviewers, set())

    def test_get_ark_config_mr_ccs(self):
        mr = mock.Mock()
        mr.description = ''
        self.assertEqual(webhook.ack_nack.get_ark_config_mr_ccs(mr), [])
        mr.description = 'Cool description.\nCc: user1 <user1@redhat.com>\n'
        self.assertEqual(webhook.ack_nack.get_ark_config_mr_ccs(mr), ['user1@redhat.com'])

    @mock.patch('webhook.ack_nack.get_ark_config_mr_ccs')
    def test_get_min_reviewers(self, mocked_getarkconfig):
        mocked_mr = mock.Mock()
        webhook.ack_nack.get_ark_config_mr_ccs.return_value = []
        # For non-ark project the function should return MIN_REVIEWERS.
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(12345, mocked_mr, ['redhat/configs/hi'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_REVIEWERS)
        self.assertFalse(cc_reviewers)
        # Ark MR that touches non-config files function should return MIN_REVIEWERS.
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(webhook.ack_nack.ARK_PROJECT_ID, mocked_mr,
                                               ['redhat/configs/hi', 'net/core/dev.c'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_REVIEWERS)
        self.assertFalse(cc_reviewers)
        # Ark MR that only touches config files and has CCs should return MIN_ARK_CONFIG_REVIEWERS.
        webhook.ack_nack.get_ark_config_mr_ccs.return_value = ['ark_user1@redhat.com']
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(webhook.ack_nack.ARK_PROJECT_ID, mocked_mr,
                                               ['redhat/configs/hi'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_ARK_CONFIG_REVIEWERS)
        self.assertEqual(cc_reviewers, set([frozenset(['ark_user1@redhat.com'])]))
        # If get_ark_config_mr_ccs() returns nothing then we expect MIN_REVIEWERS.
        webhook.ack_nack.get_ark_config_mr_ccs.return_value = []
        (min_reviewers, cc_reviewers) = \
            webhook.ack_nack.get_min_reviewers(webhook.ack_nack.ARK_PROJECT_ID, mocked_mr,
                                               ['redhat/configs/hi'])
        self.assertEqual(min_reviewers, webhook.ack_nack.MIN_REVIEWERS)
        self.assertFalse(cc_reviewers)

    def test_parse_tag(self):
        tag = webhook.ack_nack._parse_tag('Acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('LGTM!\n\nAcked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Rescind-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Rescind-acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Revoke-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Revoke-acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Rescind-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Rescind-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Revoke-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Revoke-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Unknown-tag: User 3 <user3@redhat.com>')
        self.assertIsNone(tag[0])

        tag = webhook.ack_nack._parse_tag('You forgot your Acked-by: User 1 <user1@redhat.com>')
        self.assertIsNone(tag[0])

        tag = webhook.ack_nack._parse_tag('Acked-by: Invalid Format')
        self.assertIsNone(tag[0])

        tag = webhook.ack_nack._parse_tag(('Acked-by: User 1 <user1@redhat.com>\n'
                                           '\n'
                                           '> patch title\n'
                                           '> Signed-off-by: User 2 <user2@redhat.com>\n'))
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

    @mock.patch('webhook.common.mr_code_changed', mock.Mock(return_value=True))
    def test_process_acks_nacks(self):
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user2@redhat.com',
                     'user2'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com'),
                                    ('User 2', 'user2@redhat.com')]))
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user2@redhat.com',
                     'user2'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Nacked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        msgs.append(('2021-01-09T19:50:00.000Z', 'Rescind-acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([('User 2', 'user2@redhat.com')]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user2@edhat.com',
                     'user2'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Nacked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        msgs.append(('2021-01-09T19:50:00.000Z', 'Another comment', 'user2@redhat.com', 'user2'))
        msgs.append(('2021-01-09T19:52:00.000Z', 'Revoke-nacked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Nacked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user1@redhat.com',
                     'user1'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

        # Don't allow self ACKs
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user3@redhat.com', 'user3'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 3 <user3@redhat.com>',
                     'user3@redhat.com', 'user3'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # Primary email not set on account
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user3@redhat.com', 'user3'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 3 <user3@redhat.com>',
                     'user3@redhat.com', 'user3'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-08T20:00:00.000Z', None,
                                                             'mocked', 'mocked')
        self.assertEqual(acks, set([('User 3', 'user3@redhat.com')]))
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        # This Ack should not be counted since it's before the last_commit_timestamp
        msgs.append(('2021-01-09T19:44:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-14T07:00:00.000Z', 'Yet another comment', 'user2@redhat.com',
                     'user2'))
        msgs.append(('2021-01-14T07:00:00.000Z', 'Acked-by: User 2 <user2@redhat.com>',
                     'user2@redhat.com', 'user2'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, '2021-01-10T20:00:00.000Z',
                                                             'user3@redhat.com', 'mocked', 'mocked')
        self.assertEqual(acks, set([('User 2', 'user2@redhat.com')]))
        self.assertEqual(nacks, set([]))

        # Do not fail if the payload object_attributes does not have a last_commit attribute.
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:42:00.000Z', 'Another comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:44:00.000Z', 'Nacked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Yet another comment', 'user1@redhat.com',
                     'user1'))
        msgs.append(('2021-01-09T19:48:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'user1@redhat.com', 'user1'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                             'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

        # Ignore ACK with forged address
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'someuser@gmail.com', 'someuser'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Acked-by: User 1 <user1@redhat.com>',
                     'someuser@gmail.com', 'someuser'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                             'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # Public from address not set
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'someuser@gmail.com', 'someuser'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Acked-by: User 1 <user1@redhat.com>', None,
                     'user1'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                             'mocked', 'mocked')
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([]))

        # cki-bot can impersonate users
        msgs = []
        msgs.append(('2021-01-09T19:40:00.000Z', 'Some comment', 'user1@redhat.com', 'user1'))
        msgs.append(('2021-01-09T19:46:00.000Z', 'Acked-by: User 1 <user1@redhat.com>', None,
                     'cki-bot'))
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs, None, 'user3@redhat.com',
                                                             'mocked', 'mocked')
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com')]))
        self.assertEqual(nacks, set([]))

    def test_get_ack_nack_summary(self):
        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([], [], {frozenset([])}, 2),
                         ('NeedsReview', 'Requires 2 more ACK(s).'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com')],
                                                                [], {frozenset([])}, 2),
                         ('NeedsReview',
                          'ACKed by User 1 <user1@redhat.com>. Requires 1 more ACK(s).'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [], {frozenset([])}, 2),
                         ('OK', 'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [],
                                                                {frozenset(['user1@redhat.com',
                                                                            'user2@redhat.com'])},
                                                                2),
                         ('OK',
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [],
                                                                {frozenset(['user1@redhat.com',
                                                                            'user3@redhat.com'])},
                                                                2),
                         ('OK',
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [('User 3', 'user3@redhat.com')],
                                                                {frozenset(['user1@redhat.com',
                                                                            'user2@redhat.com'])},
                                                                2),
                         ('NACKed',
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>. ' +
                          'NACKed by User 3 <user3@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com'),
                                                                 ('User 3', 'user3@redhat.com')],
                                                                [],
                                                                {frozenset(['user1@redhat.com',
                                                                            'user2@redhat.com']),
                                                                 frozenset(['user4@redhat.com',
                                                                            'user5@redhat.com'])},
                                                                2),
                         ('NeedsReview',
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>, ' +
                          'User 3 <user3@redhat.com>. Requires at least one ACK from the set(s) ' +
                          '(user4@redhat.com, user5@redhat.com).'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com'),
                                                                 ('User 3', 'user3@redhat.com')],
                                                                [],
                                                                {frozenset(['user1@redhat.com',
                                                                            'user2@redhat.com']),
                                                                 frozenset(['user4@redhat.com',
                                                                            'user5@redhat.com']),
                                                                 frozenset(['user6@redhat.com',
                                                                            'user7@redhat.com']),
                                                                 frozenset(['user1@redhat.com',
                                                                            'user8@redhat.com'])},
                                                                2),
                         ('NeedsReview',
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>, ' +
                          'User 3 <user3@redhat.com>. Requires at least one ACK from the set(s) ' +
                          '(user4@redhat.com, user5@redhat.com), ' +
                          '(user6@redhat.com, user7@redhat.com).'))

        # External email address
        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@gmail.com')],
                                                                [],
                                                                {frozenset(['user1@redhat.com',
                                                                            'user2@redhat.com'])},
                                                                2),
                         ('NeedsReview',
                          'ACKed by User 1 <user1@redhat.com>, User 2 <user2@gmail.com>. ' +
                          'Requires 1 more ACK(s).'))

    @mock.patch('webhook.common.mr_get_latest_start_sha')
    @mock.patch('webhook.common.mr_get_diff_ids')
    def test_get_filtered_changed_files(self, get_diff_ids, get_start_sha):
        gl_mergerequest = mock.Mock()
        get_diff_ids.return_value = ['1234', '5678']
        get_start_sha.return_value = 'abcd1234'
        c1 = mock.MagicMock(id="12345678")
        c2 = mock.MagicMock(id="deadbeef")
        c1.diff.return_value = [{'new_path': 'kernel/fork.c'}]
        c2.diff.return_value = [{'new_path': 'include/linux/netdevice.h'}]
        gl_mergerequest.commits.return_value = [c1, c2]
        filelist = webhook.ack_nack._get_filtered_changed_files(gl_mergerequest)
        self.assertEqual(filelist, ['include/linux/netdevice.h', 'kernel/fork.c'])
        get_start_sha.return_value = 'deadbeef'
        filelist = webhook.ack_nack._get_filtered_changed_files(gl_mergerequest)
        self.assertEqual(filelist, ['kernel/fork.c'])

    @mock.patch('webhook.ack_nack._get_filtered_changed_files')
    @mock.patch('webhook.ack_nack._run_get_maintainers')
    @mock.patch('webhook.common.mr_code_changed', mock.Mock(return_value=True))
    def test_process_merge_request1(self, get_maint, get_files):
        maint_stdout = 'User 1 <user1@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'User 2 <user2@redhat.com> (maintainer:SUBSYSTEM)\n'
        get_maint.return_value = (0, maint_stdout)
        get_files.return_value = ['include/linux/netdevice.h']

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: Impersonated User <notuser3@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 3))

        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 4, 'username': 'someuser'}
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}]}
        gl_mergerequest.labels = ['Dependencies::OK']
        gl_mergerequest.commits.return_value = \
            [mock.Mock(committed_date='2021-01-08T20:00:00.000Z')]

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(public_email=f'user{author_id}@redhat.com')

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                                   'mocked', 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: OK - '
                              'ACKed by User 1 <user1@redhat.com>, '
                              'User 2 <user2@redhat.com>.'))

        gl_mergerequest.labels = ['Dependencies::deadbeef']
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                                   'mocked', 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: OK - '
                              'ACKed by User 1 <user1@redhat.com>, '
                              'User 2 <user2@redhat.com>.'))

    @mock.patch('webhook.ack_nack._run_get_maintainers')
    @mock.patch('webhook.common.mr_code_changed', mock.Mock(return_value=True))
    def test_process_merge_request2(self, get_maint):
        maint_stdout = 'User 1 <user1@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'User 2 <user2@redhat.com> (maintainer:SUBSYSTEM)\n'
        get_maint.return_value = (0, maint_stdout)

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        # Code is pushed up based on committed_date below; previous ack is no longer valid
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-10T07:00:00.000Z', 2))

        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 4, 'username': 'someuser'}
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}]}
        gl_mergerequest.labels = ['Dependencies::OK']
        gl_mergerequest.commits.return_value = \
            [mock.Mock(committed_date='2021-01-10T03:00:00.000Z')]

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(public_email=f'user{author_id}@redhat.com')

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                                   'mocked', 'mocked', True)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:ACK/NACK Summary: NeedsReview '
                              '- ACKed by User 2 <user2@redhat.com>. Requires 1 more ACK(s).'))

    def __create_note_body(self, body, timestamp, author_id):
        ret = mock.Mock()
        ret.body = body
        ret.updated_at = timestamp
        ret.author = {'id': author_id, 'username': f'user{author_id}'}
        return ret

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message0(self, mocked_process_mr, mock_gl):
        self._test_note("request-ack-nack-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message1(self, mocked_process_mr, mock_gl):
        self._test_note("request-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message2(self, mocked_process_mr, mock_gl):
        self._test_note("Acked-by: User 1 <user1@redhat.com>", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message3(self, mocked_process_mr, mock_gl):
        # Merge request author public email will be set to user2@redhat.com below
        # so this should fail since it's a user trying to self-ack a patch.
        with self.assertLogs('cki.webhook.ack_nack', level='WARNING') as logs:
            self._test_note("Acked-by: User 2 <user2@redhat.com>", mocked_process_mr, mock_gl)
            mocked_process_mr.assert_not_called()
            self.assertIn("user2@redhat.com cannot self-ack merge request", logs.output[-1])

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message4(self, mocked_process_mr, mock_gl):
        # User's public email doesn't match the address in the Acked-by so this should fail.
        with self.assertLogs('cki.webhook.ack_nack', level='WARNING') as logs:
            self._test_note("Acked-by: User 1 <user1@gmail.com>", mocked_process_mr, mock_gl)
            mocked_process_mr.assert_not_called()
            self.assertIn(("Ignoring 'Acked-by <user1@gmail.com>' since this doesn't match "
                           "the user's public email address user1@redhat.com on GitLab"),
                          logs.output[-1])

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message5(self, mocked_process_mr, mock_gl):
        self._test_note("Some comment", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_not_called()

    def _test_note(self, note_text, mocked_process_mr, mock_gl):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = mock.Mock(author={'id': 2, 'username': 'user1'})
        gl_instance.projects.get.return_value = gl_project
        gl_instance.users.get = lambda user_id: mock.Mock(public_email=f'user{user_id}@redhat.com',
                                                          username=f'user{user_id}')
        mock_gl.return_value.__enter__.return_value = gl_instance

        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = note_text
        self.assertTrue(webhook.common.process_message("ROUTING_KEY", payload,
                                                       webhook.ack_nack.WEBHOOKS,
                                                       False, linux_src='mocked',
                                                       rhkernel_src='mocked'))

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_other_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Change a label not related to ack/nack
        msg.payload['changes']['labels']['previous'] = [{'title': 'Acks::OK'},
                                                        {'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'}]

        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked', 'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, 'mocked',
                                           'mocked', False)

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_ack_nack_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Add ack_nack label
        msg.payload['changes']['labels']['previous'] = [{'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'},
                                                       {'title': 'somethingelse::OK'}]

        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked', 'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, 'mocked',
                                           'mocked', True)

    def _create_merge_payload(self, action):
        return {'object_kind': 'merge_request',
                'user': {'id': 1, 'name': 'User 1', 'email': 'user1@redhat.com'},
                'project': {'id': 1, 'web_url': 'https://web.url/g/p'},
                'object_attributes': {'iid': 2, 'action': action,
                                      'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                'changes': {'labels': {'previous': [], 'current': []}}}

    def _test_approve_button_with(self, public_email, expected_email):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_mergerequest.notes = mock.Mock()
        gl_mergerequest.notes.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_instance.users.get.return_value = mock.Mock(public_email=public_email)

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('approved')
        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked', 'mocked'))
        gl_mergerequest.notes.create.assert_called_with(
            {'body': f'Acked-by: User 1 <{expected_email}>\n(via approve button)'})

        msg.payload = self._create_merge_payload('unapproved')
        self.assertTrue(webhook.ack_nack.process_mr_webhook(gl_instance, msg, 'mocked', 'mocked'))
        gl_mergerequest.notes.create.assert_called_with(
            {'body': f'Rescind-acked-by: User 1 <{expected_email}>\n(via unapprove button)'})

    @mock.patch('webhook.ack_nack.process_merge_request', mock.Mock())
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_approve_button(self):
        # The test payload has user1@redhat.com hardcoded as the email field. Try various
        # public email addresses.
        self._test_approve_button_with('public-email@redhat.com', 'public-email@redhat.com')
        self._test_approve_button_with('user1@redhat.com', 'user1@redhat.com')
        self._test_approve_button_with(None, 'user1@redhat.com')
        self._test_approve_button_with('', 'user1@redhat.com')
