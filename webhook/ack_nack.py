"""Process all of the ACKs/NACKs that are associated with a merge request."""
import os
import pathlib
import subprocess
import sys

from cki_lib import logger
from cki_lib import misc

from . import common

LOGGER = logger.get_logger('cki.webhook.ack_nack')
MIN_REVIEWERS = 2
MIN_ARK_CONFIG_REVIEWERS = 1
ARK_PROJECT_ID = 13604247

READY_LABEL = ('This merge request has been reviewed by Red Hat engineering and approved for'
               ' inclusion.')
NEEDS_REVIEW_LABEL = 'This merge request needs more reviews and acks from Red Hat engineering.'
NACKED_LABEL = 'This merge request has been explicitly Nacked by Red Hat engineering.'


def _run_get_maintainers(changed_file, linux_src):
    """Run the get_maintainers.pl script."""
    LOGGER.debug('Running get_maintainers.pl in directory %s', linux_src)

    # The get_maintainers.pl script requires the file to exist, so create some
    # empty files as placeholders. It's OK to leave these in place.
    dirname = os.path.dirname(changed_file)
    os.makedirs(os.path.join(linux_src, os.path.join(dirname)), exist_ok=True)
    pathlib.Path(os.path.join(linux_src, changed_file)).touch()

    os.chdir(linux_src)
    torun = ['./scripts/get_maintainer.pl', '-f', changed_file]
    ret = subprocess.run(torun, capture_output=True, check=True, text=True)
    LOGGER.debug('Executed %s, return code=%s, stdout=%s',
                 torun, ret.returncode, ret.stdout)

    return (ret.returncode, ret.stdout)


def _save(gl_project, gl_mergerequest, create_gl_status_note, status, message):
    note = f'ACK/NACK Summary: {status} - {message}'
    LOGGER.info(note)

    if status == 'OK':
        label_color = common.READY_LABEL_COLOR
        label_description = READY_LABEL
    else:
        label_color = common.NEEDS_REVIEW_LABEL_COLOR
        if status == 'NeedsReview':
            label_description = NEEDS_REVIEW_LABEL
        elif status == 'NACKed':
            label_description = NACKED_LABEL
    label = common.create_label_object(f'Acks::{status}', label_color, label_description)
    common.add_label_to_merge_request(gl_project, gl_mergerequest.iid, [label])

    if create_gl_status_note and misc.is_production():
        gl_mergerequest.notes.create({'body': note})


def _get_required_reviewers(changed_files, linux_src, submitter_email):
    """Parse the output from the get_maintainers.pl script and returns list of email addresses."""
    if not changed_files:
        return set([])

    all_reviewers = set([])

    # A merge request can span multiple subsystems so get the reviewers for each subsystem.
    # Call get_maintainers.pl individually for each file.
    for changed_file in changed_files:
        (returncode, stdout) = _run_get_maintainers(changed_file, linux_src)
        if returncode != 0:
            LOGGER.error('Error running get_maintainers.pl (%s): %s', returncode, stdout)
            # Add a bogus required reviewer so that the merge request can't be approved
            return ['ERROR_CHECK_SCRIPT_OUTPUT']

        reviewers = set([])
        for line in stdout.split('\n'):
            if not line:
                continue

            # Subsystem lists have the format like the following and should not be listed as a
            # required reviewer: 'listname@redhat.com (open list:SUBSYSTEM LIST)'
            if '<' in line:
                # First Last <user@redhat.com> (maintainer:SUBSYSTEM)
                reviewers.add(line.split('<')[1].split('>')[0])

        if reviewers:
            all_reviewers.add(frozenset(reviewers))

    # Ensure that the merge request submitter doesn't show up in a group of one
    submitter_set = frozenset([submitter_email])
    if submitter_set in all_reviewers:
        all_reviewers.remove(submitter_set)

    LOGGER.debug('Subsystem reviewers: %s', all_reviewers)

    return all_reviewers


def get_ark_config_mr_ccs(merge_request):
    """Return a list of any CC email addresses from an ark project MR description."""
    cc_list = []
    if not merge_request.description:
        return cc_list
    mlines = merge_request.description.splitlines()
    for line in mlines:
        if line.startswith('Cc: ') and line.endswith('@redhat.com>'):
            cc_list.append(line.split()[-1].split('<')[1].split('>')[0])
    return cc_list


def get_min_reviewers(project_id, merge_request, files):
    """Return the min number of reviews for the project and for ARK try to include reviewer set."""
    # For ark kernel config changes, also return users in th MR's CC line.
    if project_id == ARK_PROJECT_ID and all(file.startswith('redhat/configs/') for file in files):
        cc_reviewers = get_ark_config_mr_ccs(merge_request)
        if cc_reviewers:
            return (MIN_ARK_CONFIG_REVIEWERS, set([frozenset(cc_reviewers)]))
    return (MIN_REVIEWERS, None)


def _parse_tag(message):
    """Parse an individual message and look for any tags of interest."""
    for tag in ['Acked-by', 'Nacked-by', 'Rescind-acked-by', 'Revoke-acked-by',
                'Rescind-nacked-by', 'Revoke-nacked-by']:
        for line in message.split('\n'):
            search_tag = f'{tag}: '
            if not line.startswith(search_tag):
                continue

            name_email = line.split(search_tag)[1].split(' <')
            if len(name_email) != 2:
                continue

            name = name_email[0]
            email = name_email[1].split('>')[0]

            return (tag, name, email)

    return (None, None, None)


def _tag_email_error_message(tag, submitter_email, note_email, public_email, username):
    # cki-bot / cki-kwf-bot can impersonate other users for the approve/unapprove button.
    # redhat-patchlab leaves messages on behalf of users via the email bridge.
    if username in ('cki-bot', 'cki-kwf-bot', 'redhat-patchlab'):
        return None

    if tag in ('Acked-by', 'Rescind-acked-by', 'Revoke-acked-by') and submitter_email == note_email:
        return '%s cannot self-ack merge request' % (submitter_email)

    if not public_email:
        return (f"Ignoring '{tag} <{note_email}>' since user does not have a public "
                "email address on their GitLab profile. Click on your avatar at the top "
                "right, go to settings, and ensure that your redhat.com address is set to "
                "your public email.")

    if public_email != note_email:
        return (f"Ignoring '{tag} <{note_email}>' since this doesn't match the "
                f"user's public email address {public_email} on GitLab. You can change "
                "this by clicking on your avatar at the top right, go to settings, "
                "and ensure that your redhat.com address is set to your public email.")

    return None


def _process_acks_nacks(messages, last_commit_timestamp, submitter_email, mrequest, rhkernel_src):
    # pylint: disable=too-many-branches,too-many-locals
    """Process a list of messages and collect any ACKs/NACKS."""
    acks = set([])
    nacks = set([])
    code_changes_checked = False
    for message_timestamp, message, public_email, username in messages:
        (tag, name, note_email) = _parse_tag(message)
        if not tag:
            continue

        if not code_changes_checked:
            code_changed = common.mr_code_changed(mrequest, rhkernel_src)
            code_changes_checked = True
        if last_commit_timestamp and code_changed:
            ack_valid = last_commit_timestamp < message_timestamp
            if not ack_valid:
                LOGGER.warning("Ignoring '%s: %s <%s>' since code was changed at %s after ACKs",
                               tag, name, note_email, last_commit_timestamp)
        else:
            ack_valid = True

        nack_valid = True
        tag_error = _tag_email_error_message(tag, submitter_email, note_email, public_email,
                                             username)
        if tag_error:
            LOGGER.warning(tag_error)
            nack_valid = False
            ack_valid = False

        LOGGER.debug("Processing '%s: %s <%s>' ts=%s email=%s ack_valid=%s, nack_valid=%s",
                     tag, name, note_email, message_timestamp, public_email, ack_valid, nack_valid)

        name_email = (name, note_email)
        if tag == 'Acked-by' and ack_valid:
            acks.add(name_email)
            if name_email in nacks:
                nacks.remove(name_email)
        elif tag in ('Rescind-acked-by', 'Revoke-acked-by') and ack_valid:
            if name_email in acks:
                acks.remove(name_email)
            else:
                LOGGER.warning('Cannot find ACK to revoke for %s', name_email)
        elif tag == 'Nacked-by' and nack_valid:
            nacks.add(name_email)
        elif tag in ('Rescind-nacked-by', 'Revoke-nacked-by') and nack_valid:
            if name_email in nacks:
                nacks.remove(name_email)
            else:
                LOGGER.warning('Cannot find NACK to revoke for %s', name_email)

    return (acks, nacks)


def _show_ack_nacks(ack_nacks):
    summary = [f'{ack_nack[0]} <{ack_nack[1]}>' for ack_nack in ack_nacks]
    summary.sort()
    return ", ".join(summary)


def _get_ack_nack_summary(acks, nacks, all_reviewers, min_reviewers):
    subsystems_without_an_ack = set([])
    for subsystem_reviewers in all_reviewers:
        subsystem_has_an_ack = False
        for ack in acks:
            if ack[1] in subsystem_reviewers:
                subsystem_has_an_ack = True
                break

        if not subsystem_has_an_ack and subsystem_reviewers:
            subsystems_without_an_ack.add(subsystem_reviewers)

    summary = []
    ack_summary = _show_ack_nacks(acks)
    if ack_summary:
        summary.append(f'ACKed by {ack_summary}.')

    if subsystems_without_an_ack:
        subsys_str = []
        for subsystem_reviewers in subsystems_without_an_ack:
            # The sets are unsorted, so sort them so that we get consistent results for the tests.
            sorted_reviewers = list(subsystem_reviewers)
            sorted_reviewers.sort()
            subsys_str.append('(%s)' % (', '.join(sorted_reviewers)))

        subsys_str.sort()
        summary.append('Requires at least one ACK from the set(s) %s.' % (', '.join(subsys_str)))

    nack_summary = _show_ack_nacks(nacks)
    if nack_summary:
        summary.append(f'NACKed by {nack_summary}.')
        return ('NACKed', ' '.join(summary))

    if subsystems_without_an_ack:
        return ('NeedsReview', ' '.join(summary))

    rh_acks = [x for x in acks if x[1].endswith(('@redhat.com', '@fedoraproject.org'))]
    if len(rh_acks) < min_reviewers:
        summary.append(f'Requires {min_reviewers - len(rh_acks)} more ACK(s).')
        return ('NeedsReview', ' '.join(summary))

    return ('OK', ' '.join(summary))


def _lookup_gitlab_email(gl_instance, user_id, user_cache):
    if user_id in user_cache:
        return user_cache[user_id]

    user_cache[user_id] = gl_instance.users.get(user_id).public_email
    return user_cache[user_id]


def _lookup_submitter_and_notes(gl_instance, gl_mergerequest, user_cache):
    notes = []
    for note in gl_mergerequest.notes.list(sort='asc', order_by='created_at', as_list=False):
        email = _lookup_gitlab_email(gl_instance, note.author['id'], user_cache)
        notes.append((note.updated_at, note.body, email, note.author['username']))

    return notes


def _get_last_commit_timestamp(gl_mergerequest):
    """Walk all of the commits and get the oldest committed_date."""
    last_commit_timestamp = None
    for commit in gl_mergerequest.commits():
        if not last_commit_timestamp or last_commit_timestamp < commit.committed_date:
            last_commit_timestamp = commit.committed_date

    return last_commit_timestamp


def _get_filtered_changed_files(gl_mergerequest):
    """Walk all commits, excluding Dependencies, and build changed files list."""
    filelist = []

    diff_ids = common.mr_get_diff_ids(gl_mergerequest)
    for diff_id in diff_ids:
        diff = gl_mergerequest.diffs.get(diff_id)
        start_sha = diff.start_commit_sha

    start_sha = common.mr_get_latest_start_sha(gl_mergerequest, start_sha)

    for commit in gl_mergerequest.commits():
        if commit.id.startswith(start_sha):
            break
        diffs = commit.diff()
        for diff in diffs:
            filelist.append(diff['new_path'])

    filelist = set(filelist)
    filelist = list(filelist)
    filelist.sort()

    return filelist


def process_merge_request(gl_instance, gl_project, gl_mergerequest, linux_src, rhkernel_src,
                          create_gl_status_note):
    # pylint: disable=too-many-arguments,too-many-locals
    """Process a merge request."""
    if "Dependencies::OK" in gl_mergerequest.labels:
        changed_files = \
            [change['new_path'] for change in gl_mergerequest.changes()['changes']]
    else:
        changed_files = _get_filtered_changed_files(gl_mergerequest)

    # If get_min_reviewers() returns any reviewers just use that as our all_reviewers set.
    (min_reviewers, all_reviewers) = get_min_reviewers(gl_project.id, gl_mergerequest,
                                                       changed_files)

    user_cache = {}
    submitter_email = _lookup_gitlab_email(gl_instance, gl_mergerequest.author['id'],
                                           user_cache)

    if not all_reviewers:
        all_reviewers = _get_required_reviewers(changed_files, linux_src, submitter_email)

    notes = _lookup_submitter_and_notes(gl_instance, gl_mergerequest, user_cache)

    last_commit_timestamp = _get_last_commit_timestamp(gl_mergerequest)
    (acks, nacks) = _process_acks_nacks(notes, last_commit_timestamp, submitter_email,
                                        gl_mergerequest, rhkernel_src)

    LOGGER.debug('Changed files: %s', changed_files)
    LOGGER.debug('Minimum reviewers for this MR: %d.', min_reviewers)
    LOGGER.debug('List of possible required reviewers: %s', all_reviewers)
    LOGGER.debug('List of ACKs: %s', acks)
    LOGGER.debug('List of NACKs: %s', nacks)

    summary = _get_ack_nack_summary(acks, nacks, all_reviewers, min_reviewers)
    _save(gl_project, gl_mergerequest, create_gl_status_note, *summary)


def _approve_button_clicked(gl_instance, gl_mergerequest, msg, tag, button_name):
    # GitLab delivers in the payload an email field and it's the Email field from
    # the user's profile. This is a different field than the Public email field on the
    # profile and the two can be different. Prefer the public email address if it's set
    # since that's used elsewhere and we've told people to make their @redhat.com address
    # their public email address.
    email = _lookup_gitlab_email(gl_instance, msg.payload['user']['id'], {})
    if not email:
        email = msg.payload['user']['email']

    name_email = '%s <%s>' % (msg.payload['user']['name'], email)
    body = f'{tag}: {name_email}\n(via {button_name} button)'
    LOGGER.debug('Leaving comment %s', body)

    if misc.is_production():
        gl_mergerequest.notes.create({'body': body})


def process_mr_webhook(gl_instance, msg, linux_src, rhkernel_src):
    """Process a merge request only if a label was changed."""
    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    gl_mergerequest = gl_project.mergerequests.get(msg.payload['object_attributes']['iid'])

    gl_action = msg.payload['object_attributes'].get('action', '')
    if gl_action in ('approved', 'approval'):
        _approve_button_clicked(gl_instance, gl_mergerequest, msg, 'Acked-by', 'approve')
    elif gl_action in ('unapproved', 'unapproval'):
        _approve_button_clicked(gl_instance, gl_mergerequest, msg, 'Rescind-acked-by', 'unapprove')

    create_gl_status_note = common.has_label_changed(msg.payload, 'Acks::', True)
    process_merge_request(gl_instance, gl_project, gl_mergerequest, linux_src, rhkernel_src,
                          create_gl_status_note)

    return True


def process_note_webhook(gl_instance, msg, linux_src, rhkernel_src):
    """Process a note message only if a tag was specified."""
    notetext = msg.payload['object_attributes']['note']
    create_gl_status_note = common.force_webhook_evaluation(notetext, 'ack-nack')

    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    gl_mergerequest = gl_project.mergerequests.get(msg.payload['merge_request']['iid'])

    (tag, _, note_email) = _parse_tag(notetext)
    if tag:
        user_cache = {}
        submitter_email = _lookup_gitlab_email(gl_instance, gl_mergerequest.author['id'],
                                               user_cache)
        public_email = _lookup_gitlab_email(gl_instance, msg.payload['user']['id'], user_cache)
        tag_error = _tag_email_error_message(tag, submitter_email, note_email, public_email,
                                             msg.payload['user']['username'])
        if tag_error:
            LOGGER.warning(tag_error)
            if misc.is_production():
                gl_mergerequest.notes.create({'body': tag_error})
            return
    elif not create_gl_status_note:
        LOGGER.debug('Skipping note: %s', notetext)
        return

    process_merge_request(gl_instance, gl_project, gl_mergerequest, linux_src, rhkernel_src,
                          create_gl_status_note)


WEBHOOKS = {
    'merge_request': process_mr_webhook,
    'note': process_note_webhook,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--linux-src', **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory where get_maintainers.pl will be ran')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory where rh kernel will be checked out')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, linux_src=args.linux_src, rhkernel_src=args.rhkernel_src)


if __name__ == '__main__':
    main(sys.argv[1:])
