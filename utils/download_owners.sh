#!/usr/bin/env bash

# This script downloads the owners.yaml file from the GitLab documentation
# repository. This assumes that a readonly GitLab access token is passed
# into the script via the COM_GITLAB_RO_REPO_TOKEN environment variable.

set -e

download_gitlab_url()
{
	DEST="$1"
	MODE="$2"
	URL="$3"

	# Download the file into a temporary file within the destination
	# directory so that it'll be on the same filesystem as the destination.
	# These files will be actively in use so we want to eliminate any
	# potential race conditions.

	echo "Downloading ${URL} to ${DEST}"
	TMP=$(mktemp --tmpdir="${LINUX_SRC}")

	curl --header "Private-Token: ${COM_GITLAB_RO_REPO_TOKEN}" --silent -o "${TMP}" "${URL}"
	chmod "${MODE}" "${TMP}"
	mv "${TMP}" "${DEST}"
}

OWNERS_YAML=${OWNERS_YAML:-/data/owners.yaml}
OWNERS_YAML_URL=${OWNERS_YAML_URL:-https://gitlab.com/api/v4/projects/24455796/repository/files/info%2Fowners.yaml/raw?ref=main}

download_gitlab_url "${OWNERS_YAML}" 0644 "${OWNERS_YAML_URL}"
