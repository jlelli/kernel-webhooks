#!/usr/bin/env bash

set -e

export KERNEL_WATCH_URL=https://gitlab.com/redhat/rhel/src/kernel/watch.git
export LOCAL_REPO_PATH=/data/subsystems/kernel-watch

/code/utils/clone_kernel_watch.sh || true

exec cki_entrypoint.sh
