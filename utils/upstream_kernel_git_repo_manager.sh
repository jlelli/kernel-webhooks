#!/usr/bin/env bash

# This script sets up and maintains an upstream kernel git repo,
# with an assortment of additional upstream remotes

set -e

. cki_utils.sh

ABSPATH=${BASH_SOURCE[0]}
BASEDIR=$(dirname ${ABSPATH})
LINUX_SRC=${LINUX_SRC:-/usr/src/linux}
GIT_CACHE_DIR=/tmp
repocache="torvalds.linux"

linus="git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git"
repolist="$(pwd)/${BASEDIR}/upstream_kernel_git_repos.txt"

if [ ! -d ${LINUX_SRC} ]; then
    mkdir -p ${LINUX_SRC}
fi

cki_parse_bucket_spec 'BUCKET_CEPH_GIT_CACHE_RO'

if [ ! -d ${LINUX_SRC}/.git ]; then
    if [ -z "${AWS_ENDPOINT}" ]; then
        echo "No AWS endpoint found, bailing early, assume we're in container build phase w/o creds yet"
        exit
    fi
    echo "Cloning Linus' kernel git tree into ${LINUX_SRC}"
    #git clone ${linus} ${LINUX_SRC}
    rm -rf "${GIT_CACHE_DIR:?}/${repocache}"
    mkdir -p "${GIT_CACHE_DIR}/${repocache}"
    aws s3 --endpoint ${AWS_ENDPOINT} cp --no-progress s3://${AWS_BUCKET}/${AWS_BUCKET_PATH}${repocache}.tar ${GIT_CACHE_DIR}/
    tar -xf ${GIT_CACHE_DIR}/${repocache}.tar -C "${GIT_CACHE_DIR}/${repocache}"
    git clone --quiet "${GIT_CACHE_DIR}/${repocache}" ${LINUX_SRC}
    rm -rf "${GIT_CACHE_DIR:?}/${repocache}"
    cd ${LINUX_SRC}
    git remote set-url origin ${linus}
    git config pull.ff only
else
    echo "Updating existing kernel git tree at ${LINUX_SRC}"
fi

cd ${LINUX_SRC}

while IFS= read -r line
do
    # Skip comment lines. File format documented in ${repolist}.
    if [ $(echo ${line} | grep -c "^#") -eq 1 ]; then
        continue
    fi
    name=$(echo ${line} | cut -f1 -d=)
    url=$(echo ${line} | cut -f2 -d=)
    if [ $(git remote | grep -c ${name}) -eq 0 ]; then
        echo "Adding git remote ${name}"
        git remote add ${name} ${url}
    fi
done < "${repolist}"

echo "Pulling Linus' tree"
git pull origin
echo "And fetching all remotes:"
echo "-------------------------"
git remote -v | grep fetch
echo "-------------------------"
# omit tags, because tag collisions from different repos can be fatal
# to the job, and we don't really need them for anything but linus
git pull --no-tags --all
echo "All done."
