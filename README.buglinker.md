# Buglinker Webhook

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.buglinker \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.
