# ACK/NACK Webhook

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.ack_nack \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
	--linux-src /usr/src/linux

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

The `--linux-src` argument can also be passed to the script via the `LINUX_SRC`
environment variable.

## Verbose Logging

This webhook manages the `ack_nack` scoped label on the merge request.
Additional logging can be enabled by leaving a comment on the merge request
that starts with either `request-evaluation` or `request-ack-nack-evaluation`.
The former command will request evaluation from most webhooks.
